from selenium import webdriver

def before_all(context):
    context.browser = webdriver.Chrome()


def before_scenario(context, scenario):
    context.target_url = 'https://sprinkle-burn.glitch.me/'
    context.browser.get(context.target_url)
    assert "Worlds Best App" in context.browser.title


def after_all(context):
	context.browser.quit()
