from behave import *
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import NoSuchElementException, TimeoutException


@given(u'the user has the correct credentials')
def step_impl(context):
    context.username = 'test@drugdev.com'
    context.password = 'supers3cret'


@when(u'the user enters username')
def step_impl(context):
    # Find the input field and send the keys
    try:
        email_input = context.browser.find_element_by_xpath("//input[@name='email']")
        email_input.is_displayed()
        email_input.send_keys(context.username)
    except NoSuchElementException as msg:
        print(msg)
        raise


@when(u'the user enters password')
def step_impl(context):
    try:
        password_input = context.browser.find_element_by_xpath('//input[@name="password"]')
        password_input.is_displayed()
        password_input.send_keys(context.password)
    except NoSuchElementException as msg:
        print(msg)
        raise


@when(u'clicks Login')
def step_impl(context):
    try:
        login_button = context.browser.find_element_by_xpath('//button[text()="Login"]')
        login_button.is_displayed()
        login_button.click()
    except NoSuchElementException as msg:
        print(msg)
        raise


@then(u'the user is presented with a welcome message')
def step_impl(context):
    wait = WebDriverWait(context.browser, 5)
    # assert welcome message is displayed
    try:
        wait.until(ec.text_to_be_present_in_element((By.TAG_NAME, 'article'), 'Welcome'))
    except TimeoutException as msg:
        print("Time out exception for 'Welcome Message': {}".format(msg))
        raise

    # assert login button now is not there anymore
    login_button = None
    try:
        login_button = context.browser.find_element_by_xpath('//button[text()="Login"]')
    except NoSuchElementException:
        pass
    assert login_button is None


@given(u'the user has the incorrect credentials')
def step_impl(context):
    context.username = 'test1@drdfsugdev.com'
    context.password = 'superscret' # incorrect password


@then(u'the user is presented with an error message')
def step_impl(context):
    wait = WebDriverWait(context.browser, 5)
    # assert error message is displayed
    try:
        wait.until(ec.text_to_be_present_in_element((By.ID, 'login-error-box'), 'Credentials are incorrect'))
    except TimeoutException as msg:
        print("Time out exception for login error box: {}".format(msg))
        raise

    # assert login button and other fields are still there
    try:
        button_displayed = context.browser.find_element_by_xpath('//button[text()="Login"]').is_displayed()
        email_displayed = context.browser.find_element_by_xpath("//input[@name='email']").is_displayed()
        password_displayed = context.browser.find_element_by_xpath('//input[@name="password"]').is_displayed()
        assert (button_displayed, email_displayed, password_displayed) == (True, True, True)
    except NoSuchElementException as msg:
        print(msg)
        raise
